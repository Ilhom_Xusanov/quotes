import time
import jwt
from fastapi import HTTPException, status
from decouple import config
from .. import models

JWT_SECRET = config("secret")
JWT_ALGORITHM = config("algorithm")


def token_response(token: str):
    return {
        'access_token': token
    }


def sign_jwt_token(user: models.User):
    payload = {
        'user_id': user.id,
        'is_admin': user.is_admin,
        'expiry': time.time() + 600
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token_response(token)


def verify_token(token: str):
    decoded_token = jwt.decode(token, JWT_SECRET, algorithms=JWT_ALGORITHM)
    if decoded_token['expiry'] <= time.time():
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail=f'could not authenticate',
                            headers={"WWW-Authenticate": "Bearer"})
    return decoded_token
