from fastapi import FastAPI
from .models import Base
from .database import engine
from .routers import quotes, tags, authors, auth

app = FastAPI()

app.include_router(quotes.router)
app.include_router(tags.router)
app.include_router(authors.router)
app.include_router(auth.router)
