import datetime
from pydantic import BaseModel, constr, EmailStr


class AuthorBase(BaseModel):
    name: constr(max_length=64)
    about: str | None = None


class AuthorIn(AuthorBase):
    pass


class AuthorOut(AuthorBase):
    id: int
    created_at: datetime.datetime
    updated_at: datetime.datetime

    class Config:
        orm_mode = True


class Tag(BaseModel):
    name: constr(max_length=64)


class TagOut(Tag):
    id: int
    created_at: datetime.datetime = datetime.datetime.utcnow()
    updated_at: datetime.datetime = datetime.datetime.utcnow()

    class Config:
        orm_mode = True


class Quote(BaseModel):
    text: str
    translation: str | None = None
    author_id: int
    tags: list[Tag] = []


class QuoteOut(Quote):
    id: int
    created_at: datetime.datetime
    updated_at: datetime.datetime
    tags: list[TagOut] = []
    author: AuthorOut

    class Config:
        orm_mode = True


class User(BaseModel):
    name: str
    email: EmailStr


class UserIn(User):
    password: str


class UserLogin(BaseModel):
    email: EmailStr
    password: str

