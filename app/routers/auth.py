from fastapi import APIRouter, Depends, status, HTTPException
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer
from ..database import get_db
from app.models import User
from app import schemas
from ..auth.token_handler import sign_jwt_token, verify_token
from .. import utils, auth

router = APIRouter(
    prefix='/api/users',
    tags=['Users']
)


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    user_data = verify_token(token)
    user = db.query(User).filter(User.id==user_data.get('user_id')).first()
    return user


@router.get("/", response_model=list[schemas.User])
def get_users(db: Session = Depends(get_db)):
    users = db.query(User).order_by(User.created_at.desc()).all()
    return users


@router.post("/signup", status_code=status.HTTP_201_CREATED)
def create_user(user: schemas.UserIn, db: Session = Depends(get_db)):
    if db.query(User).filter_by(email=user.email).first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'user with email {user.email} already exists')

    hashed_password = utils.hash_password(user.dict().pop('password'))
    new_user = User(**user.dict(exclude={'password'}), password=hashed_password)
    db.add(new_user)
    db.commit()
    return sign_jwt_token(new_user)


@router.post("/login")
def user_login(user_info: schemas.UserLogin, db: Session = Depends(get_db)):
    db_user = db.query(User).filter_by(email=user_info.email).first()
    if not db_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={"message": "Invalid credentials"})

    if not utils.verify_password(user_info.password, db_user.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail={"message": "Invalid credentials"})

    return sign_jwt_token(db_user)
