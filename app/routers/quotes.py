from fastapi import APIRouter, Depends, status, Response, HTTPException
from sqlalchemy.orm import Session
from ..database import get_db
from app.models import Tag, Quote, Author, User
from app import schemas
from .auth import get_current_user

router = APIRouter(
    prefix='/api/quotes',
    tags=['Quotes']
)


@router.get("/", response_model=list[schemas.QuoteOut])
def get_quotes(db: Session = Depends(get_db), search: str | None = None):
    if search:
        tag = db.query(Tag).filter(Tag.name == search).first()
        return tag.quotes
    quotes = db.query(Quote).order_by(Quote.created_at.desc()).all()
    return quotes


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.QuoteOut)
def create_quotes(quote: schemas.Quote, db: Session = Depends(get_db),
                  current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')
    new_quote = Quote(**quote.dict(exclude={'tags'}))
    tags = quote.tags
    for tag in tags:
        tag_ = db.query(Tag).filter(Tag.name == tag.name).first()
        if tag_:
            new_quote.tags.append(tag_)
        else:
            tag_ = Tag(**tag.dict())
            db.add(tag_)
            new_quote.tags.append(tag_)
    db.add(new_quote)
    db.commit()
    db.refresh(new_quote)

    return new_quote


@router.get("/{quote_id}", response_model=schemas.QuoteOut)
def get_quote(quote_id: int, db: Session = Depends(get_db)):
    quote = db.query(Quote).filter(Quote.id==quote_id).first()
    if not quote:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'quote with id {quote_id} not found')

    return quote


@router.put("/{quote_id}", response_model=schemas.QuoteOut)
def update_quote(quote_id: int, updated_quote: schemas.Quote,
                 db: Session = Depends(get_db), current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')
    quote_query = db.query(Quote).filter(Quote.id==quote_id)
    quote = quote_query.first()
    if not quote:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'quote with id {quote_id} not found')
    tags = updated_quote.tags
    quote_query.update(updated_quote.dict(exclude_unset=True, exclude={'tags'}))
    if tags:
        for tag in tags:
            tag_ = db.query(Tag).filter(Tag.name==tag.name).first()
            if tag_:
                quote_query.first().tags.append(tag_)
            else:
                tag_ = Tag(**tag.dict())
                db.add(tag_)
                quote_query.first().tags.append(tag_)
    db.commit()

    return quote_query.first()


@router.delete("/{quote_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_quote(quote_id: int,
                 db: Session = Depends(get_db),
                 current_user: User = Depends(get_current_user)):

    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')

    quote_query = db.query(Quote).filter(Quote.id==quote_id)
    quote = quote_query.first()
    if not quote:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'quote with id {quote_id} not found')

    quote_query.delete(synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/by/{author_name}", response_model=list[schemas.QuoteOut])
def get_quotes_by_author(author_name: str, db: Session = Depends(get_db)):
    author = db.query(Author).filter(Author.name==author_name).first()
    quotes = db.query(Quote).filter(Quote.author_id==author.id).all()
    return quotes

