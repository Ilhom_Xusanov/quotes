from fastapi import APIRouter, Depends, status, Response, HTTPException
from sqlalchemy.orm import Session
from ..database import get_db
from app.models import Author, User
from app import schemas
from .auth import get_current_user

router = APIRouter(
    prefix='/api/authors',
    tags=['Authors']
)


@router.get("/", response_model=list[schemas.AuthorOut])
def get_authors(db: Session = Depends(get_db)):
    authors = db.query(Author).order_by(Author.created_at.desc()).all()
    return authors


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.AuthorOut)
def create_author(author: schemas.AuthorIn, db: Session = Depends(get_db),
                  current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')
    new_author = Author(**author.dict())
    db.add(new_author)
    db.commit()
    return new_author


@router.get("/{author_id}", response_model=schemas.AuthorOut)
def get_author(author_id: int, db: Session = Depends(get_db)):
    author = db.query(Author).filter(Author.id==author_id).first()
    if not author:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'author with id {author_id} not found')

    return author


@router.put("/{author_id}", response_model=schemas.AuthorOut)
def update_author(author: schemas.AuthorIn, author_id: int, db: Session = Depends(get_db),
                  current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')
    author_query = db.query(Author).filter(Author.id==author_id)
    author_to_update = author_query.first()
    if not author:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'author with id {author_id} not found')

    author_query.update(author.dict(exclude_unset=True))
    db.commit()

    return author_query.first()


@router.delete("/{author_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_author(author_id: int, db: Session = Depends(get_db),
                  current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')
    author_query = db.query(Author).filter(Author.id==author_id)
    author = author_query.first()
    if not author:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'author with id {author_id} not found')
    author_query.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
