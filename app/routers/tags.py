from fastapi import APIRouter, Depends, status, Response, HTTPException
from sqlalchemy.orm import Session
from ..database import get_db
from app.models import Tag, User
from app import schemas
from .auth import get_current_user

router = APIRouter(
    prefix='/api/tags',
    tags=['Tags']
)


@router.get("/", response_model=list[schemas.TagOut])
def get_tags(db: Session = Depends(get_db)):
    tags = db.query(Tag).order_by(Tag.created_at.desc()).all()
    return tags


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.TagOut)
def create_tag(tag: schemas.Tag, db: Session = Depends(get_db),
               current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')

    new_tag = Tag(**tag.dict())
    db.add(new_tag)
    db.commit()
    db.refresh(new_tag)

    return new_tag


@router.put("/{tag_id}", response_model=schemas.TagOut)
def update_tag(tag_id: int, tag: schemas.Tag, db: Session = Depends(get_db),
               current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')

    tag_query = db.query(Tag).filter(Tag.id==tag_id)
    tag_to_update = tag_query.first()
    if not tag_to_update:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'tag with id {tag_id} not found')

    tag_query.update(tag.dict(exclude_unset=True))
    db.commit()
    return tag_query.first()


@router.get("/{tag_id}", response_model=schemas.TagOut)
def get_tag(tag_id: int, db: Session = Depends(get_db)):
    tag = db.query(Tag).filter(Tag.id==tag_id).first()
    if not tag:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'tag with id {tag_id} not found')
    return tag


@router.delete("/{tag_id}")
def delete_tag(tag_id: int, db: Session = Depends(get_db),
               current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'You are not allowed to do the operation')

    tag_query = db.query(Tag).filter(Tag.id==tag_id)
    tag = tag_query.first()
    if not tag:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'tag with id {tag_id} not found')
    tag_query.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
