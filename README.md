# Title
Quotes App

# Project Description
This is the quotes app that users can read famous people's quotes, wisdom sayings

# Installation and Usage
To run the project , first of all, clone this repo to your directory    
``` git clone https://gitlab.com/jdevolop/quotes-backend.git . ``` and go to the folder you just downloaded the repo.
Create virtual environment 
``` python -m venv venv ``` and navigate where requirements.txt file is and install all dependency packages
``` pip install -r requirements.txt```. Set up all required models migrations
``` alembic upgrade head``` 
To connect to database give your own database configurations. To run server type
```uvicorn app.main:app --reload```


# Permissions
Regular users can only read quotes while admin user can create, update and delete posts.

# Authentication
Authentication is implemented through JWT, urls that can be used to obtain tokens are listed in
```api/docs/``` .
While authenticating token prefix should be
```Authorization : Bearer```
All available endpoints are listed at 
```api/docs/``` .

