"""alter column

Revision ID: 3926837af10f
Revises: 
Create Date: 2023-06-19 19:34:54.049048

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import TIMESTAMP, text


# revision identifiers, used by Alembic.
revision = '3926837af10f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('authors', 'created_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))
    op.alter_column('authors', 'updated_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))
    op.alter_column('tags', 'created_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))
    op.alter_column('tags', 'updated_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))
    op.alter_column('quotes', 'created_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))
    op.alter_column('quotes', 'updated_at', nullable=False,
                    type_=TIMESTAMP(timezone=True), server_default=text('now()'))



def downgrade() -> None:
    pass
